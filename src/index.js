const express = require('express');
const exphbs = require('express-handlebars');
const url = require('url');
const bodyParser = require('body-parser');
const cors = require('cors');
const multer = require('multer');
const fs = require('fs');
const uuidv4 = require('uuid/v4');
const session = require('express-session');
const moment = require('moment-timezone');
const mysql = require('mysql');
const bluebird = require('bluebird');
const axios = require('axios');

const db = mysql.createConnection({
   host: 'localhost',
   user: 'root',
   password: 'admin',
   database: 'proj54'
});
db.connect();

bluebird.promisifyAll(db);


//const bodyParserUrlencoded = bodyParser.urlencoded({extended: false});

const app = express();
app.use(express.static('public'));

// 查看 HTTP HEADER 的 Content-Type: application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({extended: false}));

// 查看 HTTP HEADER 的 Content-Type: application/json
app.use(bodyParser.json());

var whitelist = ['http://localhost:8080', 'http://192.168.27.42:8080', undefined, 'http://localhost:3000'];
var corsOptions = {
    credentials: true,
    origin: function (origin, callback) {
        console.log('origin: '+origin);
        if (whitelist.indexOf(origin) !== -1) {
            callback(null, true)
        } else {
            callback(new Error('Not allowed by CORS'))
        }
    }
};
app.use(cors(corsOptions));
// app.use(cors());

const upload = multer({dest: 'tmp_uploads/'});

app.use(session({
    saveUninitialized: false,
    resave: false,
    secret: 'sdgdsf ;ldkfg;ld',
    cookie: {
        maxAge: 180000
    }
}));



app.engine('hbs', exphbs({
    defaultLayout: 'main',
    extname: '.hbs',
    helpers: {
        list: require('./../helpers/list')
    }
}));

app.set('view engine', 'hbs');

// routes
app.get('/', (req, res)=>{
    const sales = require('./../data/sales.json');
    res.render('home', {
        // layout: false,
        name: sales[0].name,
    });
});

app.get('/sales', (req, res)=>{
    const sales = require('./../data/sales.json');
    res.render('sales', {
        sales: sales
    });
});

app.get('/sales2', (req, res)=>{
    const sales = require('./../data/sales.json');
    res.render('sales2', {
        sales: sales
    });
});

app.get('/try-qs', (req, res)=>{
    console.log(req.url);
    const urlParts = url.parse(req.url, true);
    console.log(urlParts);
    res.render('try_qs', {
        urlParts: urlParts
    })
});

app.post('/post-echo', (req, res)=>{
    //res.send( JSON.stringify(req.body));
    res.json(req.body);
});

app.post('/post-echo2', (req, res)=>{
    res.send(req.body.name);
});


app.get('/abc.html', (req, res)=>{
    res.send('ABC');
});


app.get('/try-upload', (req, res)=>{
    res.render('try-upload');
});

app.post('/try-upload', upload.single('avatar'), (req, res)=>{
    console.log(req.file);

    let ext = '';
    let fname = uuidv4();

    if(req.file && req.file.originalname){
        switch(req.file.mimetype){
            case 'image/png':
                ext = '.png';
            case 'image/jpeg':
                if(!ext){
                    ext = '.jpg';
                }

                fs.createReadStream(req.file.path)
                    .pipe(fs.createWriteStream(__dirname + '/../public/img/' + fname + ext));

                res.json({
                    success: true,
                    file: '/img/' + fname + ext,
                    name: req.body.name
                });
                return;
        }
    }
    res.json({
        success: false,
        file: '',
        name: req.body.name
    });

});

app.get('/upload-form1', (req, res)=>{
    res.render('upload-form1');
});

app.post('/upload-single', upload.single('filefield'), (req, res)=>{
    let ext = '';
    let fname = uuidv4();
    const result = {
        success: false,
        info: '',
        file: ''
    };

    if(req.file && req.file.originalname){
        switch(req.file.mimetype){
            case 'image/png':
                ext = '.png';
            case 'image/jpeg':
                if(!ext){
                    ext = '.jpg';
                }

                fs.createReadStream(req.file.path)
                    .pipe(fs.createWriteStream(__dirname + '/../public/img/' + fname + ext));

                res.json({
                    success: true,
                    file: '/img/' + fname + ext,
                });
                return;
            default:
                result.info = '檔案格式不符';
        }
    } else {
        result.info = '沒有選擇檔案';
    }
    res.json(result);
});

const params1 = require(__dirname + '/params-test/params');
params1(app);

const mr = require(__dirname + '/mobile_router');
app.use(mr);

const mr3 = require(__dirname + '/mobile_router3');
app.use('/mobile', mr3);

const admin3 = require(__dirname + '/admin3');
app.use('/admin3', admin3);

app.get('/try-session', (req, res)=>{
    req.session.views = req.session.views || 0;
    req.session.views ++;

    res.json({
        views: req.session.views
    })

});

app.post('/try-session', (req, res)=>{
    req.session.views = req.session.views || 0;
    req.session.views ++;

    res.json({
        views: req.session.views,
        body: req.body
    })
});

app.get('/try-moment', (req, res)=>{
    const fm = 'YYYY-MM-DD HH:mm:ss';
    const exp = req.session.cookie.expires;
    const mo1 = moment(exp);
    const mo2 = moment();

    let out = '';

    res.contentType('text/plain');

    out += mo1.format(fm) + "\n";
    out += mo2.format(fm) + "\n";
    out += mo1.tz('Europe/London').format(fm) + "\n";
    out += mo2.tz('Europe/London').format(fm) + "\n";
   res.send(out);
});

app.get('/try-db', (req, res)=>{
    let sql = "SELECT * FROM `sales`";
    db.query(sql, (error, results, fields)=>{
        //console.log(results);

        for(let s in results){
            results[s].birthday2 = moment(results[s].birthday).format('YYYY-MM-DD');
        }
        res.render('try-db', {
            sales: results
        });
    });
});

app.get('/sales3/add', (req, res)=>{
    res.render('sales3-add');
});

app.post('/sales3/add', (req, res)=>{
    const data = {
        success: false,
        message: {
            type: 'danger',
            text: ''
        }
    };
    const body = req.body;
    data.body = body;
    if(! body.sales_id || ! body.name || ! body.birthday){
        data.message.text = '資料不足';
        res.render('sales3-add', data);
        return;
    }

    db.queryAsync("INSERT INTO `sales` SET ?", body)
        .then(results=>{
            // console.log(results);
            // if(results.affectedRows===1){
            //     data.success = true;
            //     data.message.type = 'success';
            //     data.message.text = '新增成功';
            // } else {
            //     data.message.text = '資料沒有新增';
            // }
            //
            // res.render('sales3-add', data);

            if(results.affectedRows===1){
                return db.queryAsync("INSERT INTO `sales` SET ?", {
                    name: 'test',
                    sales_id: 'Hello-' + results.insertId,
                    birthday: '2000-02-02'
                })
            }
        })
        .then(results=>{
            if(results.affectedRows===1){
                data.success = true;
                data.message.type = 'success';
                data.message.text = '新增成功';
            } else {
                data.message.text = '資料沒有新增';
            }
            res.render('sales3-add', data);
        })
        .catch(error=>{


        });

});

app.get('/try-axios', (req, res)=>{

    axios.get('https://tw.yahoo.com/')
        .then(response=>{
            console.log(response);
            res.send(response.data);
        })

});




app.use((req, res)=>{
    res.type('text/html');
    res.status(404);
    res.send('找不到頁面');
});


app.listen(3000, ()=>{
    console.log('server running');
});







